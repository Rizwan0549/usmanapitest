package com.test.usmanapitest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    Retrofit retrofit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        retrofit=retrofitClient.getInstance();
        Call<List<getCommentsModel>> call=retrofit.create(apiInterface.class).getComments();
        call.enqueue(new Callback<List<getCommentsModel>>() {
            @Override
            public void onResponse(Call<List<getCommentsModel>> call, Response<List<getCommentsModel>> response) {
                Log.v("s87a8s7d",response.body().get(0).getBody());
            }

            @Override
            public void onFailure(Call<List<getCommentsModel>> call, Throwable t) {
                Log.v("s87a8s7d",t.getMessage());

            }
        });
    }
}